import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.CategoriesPage;
import pages.DashboardPage;
import pages.LoginPage;
import pages.NewCategoryPage;

/**
 * Created by alexander on 21/11/2017.
 */
public class Launcher extends BaseSeleniumTest {
    public static void main(String[] args) {
//        Разработать скрипт в виде обычного приложения (с использованием метода main():
//        1. Войти в Админ Панель
//        2. Выбрать пункт меню Каталог -> категории и дождаться загрузки страницы управления
//        категориями.
//        3. Нажать «Добавить категорию» для перехода к созданию новой категории.
//        4. После загрузки страницы ввести название новой категории и сохранить изменения. На
//        странице управления категориями должно появиться сообщение об успешном
//        создании категории.
//        5. Отфильтровать таблицу категорий по имени и дождаться там появления записи
//        созданной категории.


        EventFiringWebDriver driver = getConfiguredDriver();

        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.fillEmailInput();
        loginPage.fillPasswordInput();
        loginPage.clickSubmitButton();

        DashboardPage dashboardPage = new DashboardPage(driver);
        dashboardPage.hoverCatalog();
        dashboardPage.clickCategoriesLink();

        CategoriesPage categoriesPage = new CategoriesPage(driver);
        categoriesPage.clickCreateCategoryButton();

        NewCategoryPage newCategoryPage = new NewCategoryPage(driver);
        newCategoryPage.fillCategoryName();
        newCategoryPage.saveCategory();

        categoriesPage.checkSuccessNotification();
        categoriesPage.clickOrderCategoriesLink();
        categoriesPage.checkSuccessNotification();

        quitDriver(driver);
    }
}
