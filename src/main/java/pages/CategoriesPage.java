package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by alexander on 26/11/2017.
 */
public class CategoriesPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By createCategoryButton = By.id("page-header-desc-category-new_category");
    private By successNotification = By.className("alert-success");
    private By tableHeader = By.className("title_box");
    private By categoriesOrderLink = By.className("icon-caret-down");
    private String createdCategoryName = "newCategory";


    public CategoriesPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void clickCreateCategoryButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(createCategoryButton));
        driver.findElement(createCategoryButton).click();
    }

    public void checkSuccessNotification() {
//        wait.until(ExpectedConditions.presenceOfElementLocated(successNotification));
        Assert.assertTrue(driver.findElements(successNotification).size() > 0);
    }

    public void clickOrderCategoriesLink() {
        List<WebElement> tableHeaderElements = driver.findElements(tableHeader);
        WebElement nameHeaderElement = null;
        for(WebElement headerElement: tableHeaderElements) {
            if (headerElement.getText().equals("Имя")) {
                nameHeaderElement = headerElement;
            }
        }
        nameHeaderElement.findElement(categoriesOrderLink).click();
    }

    public void checkCreatedCategoryPresence() {
        String categoryElementsXpathSelector = String.format("//td[contains(text(),\"%s\")]", createdCategoryName);
        By categoryElements = By.xpath(categoryElementsXpathSelector);
        Assert.assertTrue(driver.findElements(categoryElements).size() > 0);
    }
}
