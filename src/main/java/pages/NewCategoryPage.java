package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexander on 26/11/2017.
 */
public class NewCategoryPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By categoryNameInput = By.id("name_1");
    private By saveCategoryButton = By.id("category_form_submit_btn");
    private String createdCategoryName = "newCategory";

    public NewCategoryPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void fillCategoryName() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoryNameInput));
        driver.findElement(categoryNameInput).sendKeys(createdCategoryName);
    }

    public void saveCategory() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(saveCategoryButton));
        driver.findElement(saveCategoryButton).click();
    }
}
