package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by alexander on 26/11/2017.
 */
public class DashboardPage {
    private EventFiringWebDriver driver;
    private WebDriverWait wait;
    private By catalogLink = By.id("subtab-AdminCatalog");
    private By categoriesLink = By.id("subtab-AdminCategories");

    public DashboardPage(EventFiringWebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);
    }

    public void hoverCatalog() {
        Actions action = new Actions(driver);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(catalogLink));
        action.moveToElement(driver.findElement(catalogLink)).build().perform();
    }

    public void clickCategoriesLink() {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(categoriesLink));
        driver.findElement(categoriesLink).click();
    }


}
